/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 1/20/14
 * Time: 9:17 AM
 *
 * Justin Brooks
 */

public class Namespace {
/*
*   Singly linked list
*   To associate names with
*   multisets
*/
    Node head;
    Integer size;
    Namespace()
    {
        head = new Node();
        head.prev = head;
        head.next = head;
        size = 0;
    }

    void addName(String name, MultiSet set)
    {
        insertLast(name, set);
    }

    void insertBefore (Node p, String name, MultiSet mult)
    {
        Node q = new Node (name, mult, p.prev, p);
        p.prev.next = q;
        p.prev = q;
        size++;
    }

    void insertAfter (Node p, String name, MultiSet mult)
    {
        Node q = new Node (name, mult, p, p.next);
        p.next.prev = q;
        p.next = q;
        size++;
    }

    void insertFirst (String name, MultiSet mult)
    {
        insertBefore (first(), name, mult);
    }

    void insertLast (String name, MultiSet mult)
    {
        insertAfter (last(), name, mult);
    }

    boolean contains(String s)
    {
        if (isEmpty()) return false;
        Node p = first();
        while (true)
        {
            if(p.name.equals(s)) return true;
            if(p.next!=first())
                p=p.next;
            else
                break;
        }
        return false;
    }

    // gets set associated with name
    MultiSet getSetFromFront(String s)
    {
        Node p = first();
        while (true)
        {
            if(p.name.equals(s)) return p.assignment;
            if(p.next!=last()) p = p.next;
            else break;
        }
        return null;
    }

    // gets set associated with name
    MultiSet getSetFromBack(String s)
    {
        Node p = last();
        while (true)
        {
            if(p.name.equals(s)) return p.assignment;
            if(p.prev!=last()) p=p.prev;
            else break;
        }
        return null;
    }

    String lastString()
    {
        return String.format("%s %d %s", last().name,
                last().assignment.size(), last().assignment);
    }

    boolean isEmpty() {return size==0;}
    Node first() {return head.next;}
    Node last() {return head.prev;}
}
