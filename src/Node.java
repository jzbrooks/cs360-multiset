/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 1/19/14
 * Time: 7:51 PM
 *
 * Justin Brooks
 */

public class Node {
    // For multiset node
    Node next,prev;
    int multiplicity, num;
    Node(int data, int mult, Node prv, Node nxt){num=data; multiplicity=mult; next=nxt; prev=prv;}

    // For namespace node
    String name;
    MultiSet assignment;
    Node(String n, MultiSet assn, Node prv, Node nxt){name=n; assignment=assn; next=nxt; prev=prv;}

    // Default constructor
    Node(){}
}
