/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 1/18/14
 * Time: 10:08 AM
 *
 * Justin Brooks
 */

public class MultiSet {
/*
*   Circular doubly linked list
*   w/ header node to represent
*   a multi set and allow for
*   sorted insertion
*/

    Node head;
    Integer size;
    MultiSet()
    {
        head = new Node();
        head.prev = head;
        head.next = head;
        size = 0;
    }

    // sorted insert (ascending by x)
    void insert(int x, int mult)
    {
        if (size<1)
        {
            insertFirst(x, mult);
        }

        else{
            if(find(x))
            {
                Node p = findPosition(x);
                p.multiplicity += mult;
                size += mult;
            }
            else
            {
                boolean inserted = false;
                Node p = first();
                do {
                    if(p.num < x)
                    {
                        if (p!=last())
                            p = p.next;
                        else
                        {
                            insertLast(x,mult);
                            inserted = true;
                        }
                    }
                    else {
                        insertBefore(p,x,mult);
                        inserted = true;
                    }
                } while(!inserted);

            }
        }
    }

    void insertBefore (Node p, int x, int mult)
    {
        Node q = new Node (x, mult, p.prev, p);
        p.prev.next = q;
        p.prev = q;
        size += mult;
    }

    void insertAfter (Node p, int x, int mult)
    {
        Node q = new Node (x, mult, p, p.next);
        p.next.prev = q;
        p.next = q;
        size += mult;
    }

    void insertFirst (int x, int mult)
    {
        insertBefore (first(), x, mult);
    }

    void insertLast (int x, int mult)
    {
        insertAfter (last(), x, mult);
    }

    boolean find(int x)
    {
        Node p = first();
        for (int i=0; i<size; i++){
            if (p.num == x) return true;
            p = p.next;
        }
        return false;
    }

    Node findPosition(int x)
    {
        Node p = first();
        for (int i=0; i<size; i++)
        {
            if(p.num == x) return p;
            p = p.next;
        }
        return null;
    }

    // converts a string to a multiset
    void parseAggregate(String s)
    {
        Integer n, m;
        String value_string;
        int mult_operator_loc, last_delimiter = 1;
        if(!s.equals("{}"))
        {
            for (int i=1; i<s.length(); i++)
            {
                if(s.charAt(i)==',' || s.charAt(i)=='}'){
                    value_string = s.substring(last_delimiter,i);
                    if(value_string.contains("*")){
                        last_delimiter=i+1;
                        mult_operator_loc = value_string.indexOf('*');
                        n = Integer.parseInt(value_string.substring(0,mult_operator_loc));
                        m = Integer.parseInt(value_string.substring
                                (mult_operator_loc+1,value_string.length()));
                        if(m!=0) insert(n, m);
                    }
                    if(!value_string.contains("*")){
                        last_delimiter = i+1;
                        n = Integer.parseInt(value_string);
                        insert(n, 1);
                    }
                }
            }
        }
    }

    @Override
    public String toString()
    {
        if(isEmpty()) return "{}";
        else
        {
            String str = "{";
            Node p = first();
            while(p.next!=first())
            {
                str += String.format("%d*%d",p.num,p.multiplicity);
                if (p!=last())
                    str += ",";
                p = p.next;
            }
            str += "}";
            return str;
        }
    }

    Node first() {return head.next;}
    Node last() {return head.prev;}
    boolean isEmpty() {return size==0;}
    int size() {return size;}
}

