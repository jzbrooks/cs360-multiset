import java.io.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 1/19/14
 * Time: 7:51 PM
 *
 * Justin Brooks
 */

public class Main {
    public static void main(String[] args)
    {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        String line_in = null;
        String save = "", accumulator = "";
        String var = "", instruction1 = "", instruction2 = "", rhs = "";

        Namespace namespace = new Namespace();
        MultiSet set;

        // runs until an empty line is encountered
        while(true)
        {
            set = new MultiSet();
            int location = 0;
            try {
                line_in = in.readLine();
                if (line_in==null) return;
            }catch (IOException ex) {
                System.out.println("Error reading input");
            }

            for(int i=0; i < line_in.length(); i++)
            {
                accumulator += line_in.charAt(i);
                if (line_in.charAt(i)==' ')
                {
                    save = accumulator.substring(0, accumulator.length()).trim();
                    accumulator = "";
                    location++;
                }
                if(i == line_in.length()-1)
                {
                    save = accumulator;
                    accumulator = "";
                    location++;
                }

                // save by position of each item in read in string
                if(location==0) {}
                else if(location==1) {var = save;}
                else if(location==2) {instruction1 = save;}
                else if(location==3) {rhs = save;}
                else {System.out.println("Input Format Error");}
            }


            if (rhs.charAt(0) == '{')
            {
                set.parseAggregate(rhs);
                namespace.addName(var, set);
            }
            else
            {
                if(namespace.contains(rhs))
                {
                    try {
                        namespace.addName(var,namespace.getSetFromBack(rhs));
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            System.out.println(namespace.lastString());
        }
    }
}

